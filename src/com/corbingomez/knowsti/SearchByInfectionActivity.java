package com.corbingomez.knowsti;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

public class SearchByInfectionActivity extends ActionBarActivity {

	public String googleSearchURL = "https://www.google.com/#q=";
	
	public String searchChancroid = googleSearchURL + "chancroid";
	public String searchChlamydia = googleSearchURL + "chlamydia";
	public String searchCytomegalovirus = googleSearchURL + "cytomegalovirus";
	public String searchGenitalWarts = googleSearchURL + "genital+warts";
	public String searchGonorrhea = googleSearchURL + "gonorrhea";
	public String searchHepatitisB = googleSearchURL + "hepatitis+b";
	public String searchHerpes = googleSearchURL + "herpes";
	public String searchHivAids = googleSearchURL + "hiv+aids";
	public String searchHpv = googleSearchURL + "hpv";
	public String searchMolluscumContagiosum = googleSearchURL + "molluscum+contagiosum";
	public String searchPid = googleSearchURL + "pelvic+inflammatory+disease";
	public String searchPubicLice = googleSearchURL + "pubic+lice";
	public String searchScabies = googleSearchURL + "scabies";
	public String searchSyphilis = googleSearchURL + "syphilis";
	public String searchTrich = googleSearchURL + "trichomoniasis";
	
	Button searchButton;
	
	CheckBox chancroidCheckBox;
	CheckBox chlamydiaCheckBox;
	CheckBox cytomegalovirusCheckBox;
	CheckBox genitalWartsCheckBox;
	CheckBox gonorrheaCheckBox;
	CheckBox hepatitisBCheckBox;
	CheckBox herpesCheckBox;
	CheckBox hivAidsCheckBox;
	CheckBox hpvCheckBox;
	CheckBox molluscumContagiosumCheckBox;
	CheckBox pidCheckBox;
	CheckBox pubicLiceCheckBox;
	CheckBox scabiesCheckBox;
	CheckBox syphilisCheckBox;
	CheckBox trichCheckBox;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_by_infection);
		
		searchButton = (Button)findViewById(R.id.searchButton);
		searchButton.setOnClickListener((android.view.View.OnClickListener) searchButtonListener);
		
		chancroidCheckBox = (CheckBox)findViewById(R.id.chancroidCheckBox);
		chlamydiaCheckBox = (CheckBox)findViewById(R.id.chlamydiaCheckBox);
		cytomegalovirusCheckBox = (CheckBox)findViewById(R.id.cytomegalovirusCheckBox);
		genitalWartsCheckBox = (CheckBox)findViewById(R.id.genitalWartsCheckBox);
		gonorrheaCheckBox = (CheckBox)findViewById(R.id.gonorrheaCheckBox);
		hepatitisBCheckBox = (CheckBox)findViewById(R.id.hepatitisBCheckBox);
		herpesCheckBox = (CheckBox)findViewById(R.id.herpesCheckBox);
		hivAidsCheckBox = (CheckBox)findViewById(R.id.hivAidsCheckBox);
		hpvCheckBox = (CheckBox)findViewById(R.id.hpvCheckBox);
		molluscumContagiosumCheckBox = (CheckBox)findViewById(R.id.molluscumContagiosumCheckBox);
		pidCheckBox = (CheckBox)findViewById(R.id.pidCheckBox);
		pubicLiceCheckBox = (CheckBox)findViewById(R.id.pubicLiceCheckBox);
		scabiesCheckBox = (CheckBox)findViewById(R.id.scabiesCheckBox);
		syphilisCheckBox = (CheckBox)findViewById(R.id.syphilisCheckBox);
		trichCheckBox = (CheckBox)findViewById(R.id.trichCheckBox);

	}
	
	public OnClickListener searchButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			if(chancroidCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchChancroid));
				startActivity(browserIntent);
			}
			
			if(chlamydiaCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchChlamydia));
				startActivity(browserIntent);
			}
			
			
			if(cytomegalovirusCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchCytomegalovirus));
				startActivity(browserIntent);
			}
			
			if(genitalWartsCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchGenitalWarts));
				startActivity(browserIntent);
			}
			
			if(gonorrheaCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchGonorrhea));
				startActivity(browserIntent);
			}
			
			if(hepatitisBCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchHepatitisB));
				startActivity(browserIntent);
			}
			
			if(herpesCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchHerpes));
				startActivity(browserIntent);
			}
			
			if(hivAidsCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchHivAids));
				startActivity(browserIntent);
			}
			
			if(hpvCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchHpv));
				startActivity(browserIntent);
			}
			
			if(molluscumContagiosumCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchMolluscumContagiosum));
				startActivity(browserIntent);
			}
			
			if(pidCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchPid));
				startActivity(browserIntent);
			}
			
			if(pubicLiceCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchPubicLice));
				startActivity(browserIntent);
			}
			
			if(scabiesCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchScabies));
				startActivity(browserIntent);
			}
			
			if(syphilisCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchSyphilis));
				startActivity(browserIntent);
			}
			
			if(trichCheckBox.isChecked()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchTrich));
				startActivity(browserIntent);
			}
			
			
			
		}
		
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_by_infection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
