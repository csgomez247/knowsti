package com.corbingomez.knowsti;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

public class SearchBySymptomActivity extends ActionBarActivity {

	static final String EXTRA_INFECTIONS = "com.corbingomez.EXTRA_INFECTIONS";
	
	Button searchInfectionsButton;
	
	CheckBox genitalSoresCheckBox;
	CheckBox noSymptomsCheckBox;
	CheckBox genitalWartsCheckBox;
	CheckBox soresOnTheMouthCheckBox;
	CheckBox thrushCheckBox;
	CheckBox vaginitisCheckBox;
	CheckBox bruisingCheckBox;
	CheckBox discoloredGrowthCheckBox;
	CheckBox unexpectedBleedingCheckBox;
	CheckBox unusualRashCheckBox;
	CheckBox bumpsWithIndentsCheckBox;
	CheckBox itchingCheckBox;
	CheckBox liceAndEggsCheckBox;
	CheckBox unpleasantDischargeCheckBox;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_by_symptom);
		
		searchInfectionsButton = (Button)findViewById(R.id.searchButton);
		searchInfectionsButton.setOnClickListener((android.view.View.OnClickListener) searchInfectionsButtonListener);
		
		genitalSoresCheckBox = (CheckBox)findViewById(R.id.imageSearchCheckBox);
		noSymptomsCheckBox = (CheckBox)findViewById(R.id.chlamydiaCheckBox);
		genitalWartsCheckBox = (CheckBox)findViewById(R.id.cytomegalovirusCheckBox);
		soresOnTheMouthCheckBox = (CheckBox)findViewById(R.id.genitalWartsCheckBox);
		thrushCheckBox = (CheckBox)findViewById(R.id.gonorrheaCheckBox);
		vaginitisCheckBox = (CheckBox)findViewById(R.id.hepatitisBCheckBox);
		bruisingCheckBox = (CheckBox)findViewById(R.id.herpesCheckBox);
		discoloredGrowthCheckBox = (CheckBox)findViewById(R.id.hivAidsCheckBox);
		unexpectedBleedingCheckBox = (CheckBox)findViewById(R.id.hpvCheckBox);
		unusualRashCheckBox = (CheckBox)findViewById(R.id.molluscumContagiosumCheckBox);
		bumpsWithIndentsCheckBox = (CheckBox)findViewById(R.id.pidCheckBox);
		itchingCheckBox = (CheckBox)findViewById(R.id.pubicLiceCheckBox);
		liceAndEggsCheckBox = (CheckBox)findViewById(R.id.liceAndEggsCheckBox);
		unpleasantDischargeCheckBox = (CheckBox)findViewById(R.id.unpleasantDischargeCheckBox);

	}
	
	public OnClickListener searchInfectionsButtonListener = new OnClickListener() {

		@SuppressWarnings("unused")
		@Override
		public void onClick(View v) {
			int chancroidPts = 0;
			int chlamydiaPts = 0;
			int cytomegalovirusPts = 0;
			int genitalWartsPts = 0;
			int gonorrheaPts = 0;
			int hepatitisBPts = 0;
			int herpesPts = 0;
			int hivAidsPts = 0;
			int hpvPts = 0;
			int molluscumContagiosumPts = 0;
			int pidPts = 0;
			int pubicLicePts = 0;
			int scabiesPts = 0;
			int syphilisPts = 0;
			int trichPts = 0;
			
			if(genitalSoresCheckBox.isChecked()) {
				chancroidPts++;
				herpesPts++;
				scabiesPts++;
			}
			
			if(noSymptomsCheckBox.isChecked()) {
				chlamydiaPts++;
				cytomegalovirusPts++;
				gonorrheaPts++;
				hepatitisBPts++;
				hivAidsPts++;
				hpvPts++;
				pidPts++;
				syphilisPts++;
				trichPts++;
			}
			
			if(genitalWartsCheckBox.isChecked()) {
				genitalWartsPts++;
			}
			
			if(soresOnTheMouthCheckBox.isChecked()) {
				herpesPts++;
				syphilisPts++;
			}
			
			if(thrushCheckBox.isChecked()) {
				hivAidsPts++;
			}
			
			if(vaginitisCheckBox.isChecked()) {
				trichPts++;
				hivAidsPts++;
			}
			
			if(bruisingCheckBox.isChecked()) {
				hivAidsPts++;
			}
			
			if(discoloredGrowthCheckBox.isChecked()) {
				hivAidsPts++;
			}
			
			if(unexpectedBleedingCheckBox.isChecked()) {
				hivAidsPts++;
			}
			
			if(unusualRashCheckBox.isChecked()) {
				scabiesPts++;
				syphilisPts++;
				herpesPts++;
				gonorrheaPts++;
			}
			
			if(bumpsWithIndentsCheckBox.isChecked()) {
				molluscumContagiosumPts++;
			}
			
			if(itchingCheckBox.isChecked()) {
				scabiesPts++;
				pubicLicePts++;
				gonorrheaPts++;
			}
			
			if(liceAndEggsCheckBox.isChecked()) {
				pubicLicePts++;
			}
			
			if(unpleasantDischargeCheckBox.isChecked()) {
				trichPts++;
				chlamydiaPts++;
				gonorrheaPts++;
			}
			
			
			HashMap<String, Integer> infectionList = new HashMap<String, Integer>();
			ValueComparator valComp = new ValueComparator(infectionList);
			TreeMap<String, Integer> sortedInfectionMap = new TreeMap<String, Integer>(valComp);
			
			infectionList.put("Chancroid", chancroidPts);
			infectionList.put("Chlamydia", chlamydiaPts);
			infectionList.put("Cytomegalovirus", cytomegalovirusPts);
			infectionList.put("Genital Warts", genitalWartsPts);
			infectionList.put("Gonorrhea", gonorrheaPts);
			infectionList.put("Hepatitis B", hepatitisBPts);
			infectionList.put("Herpes", herpesPts);
			infectionList.put("HIV/AIDS", hivAidsPts);
			infectionList.put("Human Papillomavirus", hpvPts);
			infectionList.put("Molluscum Contagiosum", molluscumContagiosumPts);
			infectionList.put("Pelvic Inflammatory Disease", pidPts);
			infectionList.put("Pubic Lice", pubicLicePts);
			infectionList.put("Syphilis", chancroidPts);
			infectionList.put("Trichomoniasis", trichPts);
			
			sortedInfectionMap.putAll(infectionList);
			
			ArrayList<String> infections = new ArrayList<String>();
			for(String infection : sortedInfectionMap.keySet()) {
				if(infectionList.get(infection) > 0) {
					infections.add(infection);
				}
			}
			
			Intent intent = new Intent(SearchBySymptomActivity.this, ViewInfectionsActivity.class);
			intent.putExtra(EXTRA_INFECTIONS, infections);
			startActivity(intent);
			
		}	
	};
	
	class ValueComparator implements Comparator<String> {

		Map<String, Integer> base;
	    public ValueComparator(Map<String, Integer> base) {
	        this.base = base;
	    }
		
		@Override
		public int compare(String lhs, String rhs) {
			if(base.get(lhs) >= base.get(rhs)) {
				return -1;
			}
			else {
				return 1;
			}
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_by_symptom, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
