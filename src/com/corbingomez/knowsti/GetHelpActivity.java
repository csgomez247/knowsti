package com.corbingomez.knowsti;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

public class GetHelpActivity extends ActionBarActivity {
	
	public static final String plannedParenthoodURL ="http://www.plannedparenthood.org/health-topics/stds-hiv-safer-sex-101.htm";
	public static final String cdcURL = "http://www.cdc.gov/STD/";
	public static final String worldHealthOrganizationURL = "http://www.who.int/topics/sexually_transmitted_infections/en/";
	
	Button goToLinksButton;
	
	CheckBox plannedParenthoodCheckBox;
	CheckBox cdcCheckBox;
	CheckBox worldHealthOrganizationCheckBox;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_help);
		
		goToLinksButton = (Button)findViewById(R.id.goToLinksButton);
		goToLinksButton.setOnClickListener((android.view.View.OnClickListener) goToLinksButtonListener);
		
		plannedParenthoodCheckBox = (CheckBox)findViewById(R.id.cytomegalovirusCheckBox);
		cdcCheckBox = (CheckBox)findViewById(R.id.cdcCheckBox);
		worldHealthOrganizationCheckBox = (CheckBox)findViewById(R.id.worldHealthOrganizationCheckBox);
	}
	
	public OnClickListener goToLinksButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(plannedParenthoodCheckBox.isChecked()) {
				Intent plannedParenthoodIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(plannedParenthoodURL));
				startActivity(plannedParenthoodIntent);
			}
			
			if(cdcCheckBox.isChecked()) {
				Intent cdcIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(cdcURL));
				startActivity(cdcIntent);
			}
			
			if(worldHealthOrganizationCheckBox.isChecked()) {
				Intent worldHealthOrganizationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(worldHealthOrganizationURL));
				startActivity(worldHealthOrganizationIntent);
			}
		}	
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.get_help, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
