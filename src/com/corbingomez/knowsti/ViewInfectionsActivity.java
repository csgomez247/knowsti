package com.corbingomez.knowsti;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

public class ViewInfectionsActivity extends ActionBarActivity {
	
	private TableLayout infectionTableScrollView;
	ArrayList<String> infections;
	
	Button moreInfectionsButton;
	Button getHelpButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_infections);
		
		moreInfectionsButton = (Button)findViewById(R.id.moreInfectionsButton);
		getHelpButton = (Button)findViewById(R.id.getHelpButton);
		
		moreInfectionsButton.setOnClickListener((android.view.View.OnClickListener) moreInfectionsButtonListener);
		getHelpButton.setOnClickListener((android.view.View.OnClickListener) getHelpButtonListener);	
		
		infectionTableScrollView = (TableLayout)findViewById(R.id.infectionTableScrollView);
		
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			infections = extras.getStringArrayList("com.corbingomez.EXTRA_INFECTIONS");
		}
		
		int arrayIndex = 0;
		
		for(String infection : infections) {
			insertInfectionInScrollView(infection, arrayIndex);
			arrayIndex++;
		}
	}

	public OnClickListener getHelpButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(ViewInfectionsActivity.this, GetHelpActivity.class);
			startActivity(intent);
		}	
	};
	
	public OnClickListener moreInfectionsButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(ViewInfectionsActivity.this, SearchByInfectionActivity.class);
			startActivity(intent);
		}	
	};
	
	private void insertInfectionInScrollView(String infection, int arrayIndex) {
		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View newInfectionRow = inflater.inflate(R.layout.infection_row, null);
		TextView newInfectionTextView = (TextView)newInfectionRow.findViewById(R.id.infectionTextView);
		newInfectionTextView.setText(infection);
		infectionTableScrollView.addView(newInfectionRow, arrayIndex);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_infections, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
